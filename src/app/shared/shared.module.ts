import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatLegacyCardModule as MatCardModule } from '@angular/material/legacy-card';
import { MatLegacyTabsModule as MatTabsModule } from '@angular/material/legacy-tabs';
import { RouterModule } from '@angular/router';
import { ResponsiveImageComponent } from './responsive-image/responsive-image.component';
import { PaymentComponent } from './payment/payment.component';
import { QuestionAndAnswerComponent } from './payment/question-and-answer/question-and-answer.component';

@NgModule({
  declarations: [ResponsiveImageComponent, PaymentComponent, QuestionAndAnswerComponent],
  exports: [ResponsiveImageComponent, PaymentComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatCardModule,
    MatTabsModule,
    RouterModule,
  ],
})
export class SharedModule {}
