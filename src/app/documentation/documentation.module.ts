import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ScullyLibModule } from '@scullyio/ng-lib';
import { DocumentationRoutingModule } from './documentation-routing.module';
import { MatLegacyCardModule as MatCardModule } from '@angular/material/legacy-card';
import { DocumentationIndexComponent } from './documentation-index/documentation-index.component';
import { DocumentationPageComponent } from './documentation-page/documentation-page.component';
import { MatLegacyButtonModule as MatButtonModule } from '@angular/material/legacy-button';

@NgModule({
  declarations: [DocumentationIndexComponent, DocumentationPageComponent],
  imports: [
    CommonModule,
    DocumentationRoutingModule,
    ScullyLibModule,
    MatCardModule,
    MatButtonModule,
  ],
})
export class DocumentationModule {}
