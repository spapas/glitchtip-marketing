import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatLegacyCardModule as MatCardModule } from '@angular/material/legacy-card';
import { ScullyLibModule } from '@scullyio/ng-lib';
import { SDKDocsIndexComponent } from './sdk-docs-index.component';
import { SDKDocsRoutingModule } from './sdk-docs-routing.module';
import { SDKDocsComponent } from './sdk-docs.component';

@NgModule({
  declarations: [SDKDocsIndexComponent, SDKDocsComponent],
  imports: [CommonModule, SDKDocsRoutingModule, ScullyLibModule, MatCardModule],
})
export class SDKDocsModule {}
