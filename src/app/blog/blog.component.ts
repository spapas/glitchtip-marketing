import { Component, ViewEncapsulation } from '@angular/core';
import { ScullyRoutesService } from '@scullyio/ng-lib';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css'],
  encapsulation: ViewEncapsulation.Emulated,
  preserveWhitespaces: true,
})
export class BlogComponent {
  current$ = this.scully.getCurrent();
  title$ = this.current$.pipe(map((route) => route.title));
  author$ = this.current$.pipe(map((route) => route.author));
  date$ = this.current$.pipe(map((route) => route.date));

  constructor(private scully: ScullyRoutesService) {}

  ngAfterViewInit() {
    if (!navigator.webdriver) {
      (function (d: Document, t) {
        let g = d.createElement(t) as HTMLScriptElement;
        let s = d.getElementsByTagName(t)[0];
        g.src = 'https://chirpy.dev/bootstrap/comment.js';
        g.dataset.chirpyDomain = 'glitchtip.com';
        s.parentNode?.insertBefore(g, s);
      })(document, 'script');
    }
  }
}
