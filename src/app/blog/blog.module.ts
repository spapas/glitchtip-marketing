import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ScullyLibModule } from '@scullyio/ng-lib';
import { BlogRoutingModule } from './blog-routing.module';
import { BlogComponent } from './blog.component';
import { MatLegacyCardModule as MatCardModule } from '@angular/material/legacy-card';
import { BlogIndexComponent } from './blog-index/blog-index.component';

@NgModule({
  declarations: [BlogComponent, BlogIndexComponent],
  imports: [CommonModule, BlogRoutingModule, ScullyLibModule, MatCardModule]
})
export class BlogModule {}
